import sys
import winput
from PySide2.QtCore import Qt
from PySide2.QtWidgets import QApplication, QMainWindow, QRadioButton, QGroupBox
from PySide2.QtWidgets import QVBoxLayout, QHBoxLayout, QWidget, QLabel, QSpinBox
from PySide2.QtWidgets import QAction, QCheckBox
from PySide2.QtGui import QIcon
from PySide2.QtMultimedia import QSound

WM_MOUSEWHEEL = 0x020A # mouse wheel moved

class ArtyWidget(QWidget):

    def __init__(self, parent=None):
        super().__init__(parent)

        self.audio_effect_10 = QSound("sound1.wav")
        self.audio_effect_10b = QSound("sound1b.wav")
        self.audio_effect_50 = QSound("sound2.wav")

        self.radio_type1 = QRadioButton("Type 1: MILRAD 622 - 978")
        self.radio_type2 = QRadioButton("Type 2: MILRAD 800 - 1120")
        self.sound_cbox = QCheckBox("Sound", checked=True)

        gbox_layout = QVBoxLayout()
        gbox_layout.addWidget(self.radio_type1)
        gbox_layout.addWidget(self.radio_type2)
        gbox_layout.addWidget(self.sound_cbox)

        self.gbox = QGroupBox("Settings (Ctrl+H to hide the window bar)")
        self.gbox.setLayout(gbox_layout)

        self.target_range = QSpinBox()
        self.target_range.setMinimum(100)
        self.target_range.setMaximum(1600)
        self.elevation = QSpinBox()
        self.elevation.setReadOnly(True)
        
        font = self.elevation.font()
        font.setPointSize(14)
        self.target_range.setFont(font)
        self.elevation.setFont(font)

        self.radio_type1.toggled.connect(self.toggle_type)
        self.radio_type1.setChecked(True)
        self.target_range.valueChanged.connect(self.update_elevation)

        layout = QHBoxLayout()
        layout.addWidget(QLabel("Range:"))
        layout.addWidget(self.target_range)
        layout.addWidget(QLabel("Elevation:"))
        layout.addWidget(self.elevation)

        main_layout = QVBoxLayout()
        main_layout.addWidget(self.gbox)
        main_layout.addLayout(layout)

        self.setLayout(main_layout)

    def toggle_type(self):
        if self.radio_type1.isChecked():
            self.elevation.setMinimum(622)
            self.elevation.setMaximum(978)
        else:
            self.elevation.setMinimum(800)
            self.elevation.setMaximum(1120)

        self.update_elevation(self.target_range.value())

    def update_elevation(self, value):

        x0, x1 = 100, 1600
        if self.radio_type1.isChecked():
            y0, y1 = 978, 622
        else:
            y0, y1 = 1120, 800

        elevation = round(self.interpolator(value, x0, x1, y0, y1))
        self.elevation.setValue(elevation)

    def interpolator(self, x, x0, x1, y0, y1):
        return y0 + (y1 - y0) * (x - x0) / (x1 - x0)

    def increment_range(self, mouse_event):
        if mouse_event.action == WM_MOUSEWHEEL:
            current_range = self.target_range.value()
            increment = mouse_event.additional_data
            set_range = current_range + increment
            self.target_range.setValue(set_range)
            self.play_click(set_range, increment)

    def play_click(self, set_range, increment):
        if self.sound_cbox.isChecked():
            if set_range % 50 == 0:
                self.audio_effect_50.play()
            elif set_range % 10 == 0:
                if increment > 0:
                    self.audio_effect_10.play()
                else:
                    self.audio_effect_10b.play()


class ArtyGUI(QMainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.arty_widget = ArtyWidget(self)

        self.setCentralWidget(self.arty_widget)
        self.setWindowTitle("Arty")
        self.setWindowIcon(QIcon("icon.png"))
        self.setWindowFlag(Qt.WindowStaysOnTopHint)
        self.resize(150, 20)

        self.hideBarAction = QAction('&Hide Window Bar', self)
        self.hideBarAction.setShortcut('Ctrl+H')
        self.hideBarAction.triggered.connect(self.hide_bar)
        self.addAction(self.hideBarAction)

        self.quitAction = QAction('&Quit', self)
        self.quitAction.setShortcut('Ctrl+Q')
        self.quitAction.triggered.connect(qApp.quit)
        self.addAction(self.quitAction)

        self.arty_widget.target_range.setFocus()

    def hide_bar(self):
        self.setWindowFlag(Qt.FramelessWindowHint)
        self.arty_widget.gbox.setTitle("Settings (Ctrl+Q to quit)")
        self.show()
        self.arty_widget.target_range.setFocus()


if __name__ == "__main__":
    app = QApplication(sys.argv)

    arty_gui = ArtyGUI()
    arty_gui.show()

    winput.hook_mouse(arty_gui.arty_widget.increment_range)

    sys.exit(app.exec_())
