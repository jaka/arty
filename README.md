# Arty

Artilery interpolator widget for playing Hell Let Loose

# Requisites

* Windows (I'm so sorry)
* Python 3

## Python libraries

* PySide2
* winput

# Install

1.  Download and install [Python](https://www.python.org/downloads/). Use the following options:
    - [x] PyLauncher
    - [x] Add Pyhton to PATH
    - [x] Install pip
2.  Log out and log in to your user account to load Python binaries into your PATH variable
3.  Install PySide2 in shell `pip install pyside2`
4.  Install winput in shell `pip install winput`
5.  Clone / download this repository
6.  Double-click `arty.pyw`

# Tips

* The widget stays on top of other applications - you can have it next to the elevation indicator.
* When widget is not in focus you can use mouse wheel to increment the range input.
* Using mouse wheel inside a range input spin-box when widget is not focus increments double.
* `Ctrl+H` to hide the window bar.
* `Ctrl+Q` to close the application.